package com.cgm.academy.hibernate_first;

import com.cgm.academy.hibernate_first.person.controller.PersonController;
import com.cgm.academy.hibernate_first.person.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@SpringBootApplication
public class HibernateFirstApplication implements CommandLineRunner {

    private final PersonController personController;

    @Autowired
    public HibernateFirstApplication(PersonController personController) {
        this.personController = personController;
    }

    public static void main(String[] args) {
        SpringApplication.run(HibernateFirstApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        IntStream.range(0,20).forEach(i->{
            String random = UUID.randomUUID().toString();
            LocalDate localDate = LocalDate.now().minusYears(new Random().nextInt(25))
                                    .minusDays(new Random().nextInt(180));
            Date dob = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            Person person = new Person("Joe"+random, "Doe"+random, dob);
            personController.savePerson(person);
        });

        personController.getAllPerson().forEach(System.out::println);
    }
}
