package com.cgm.academy.hibernate_first.person.repository;

import com.cgm.academy.hibernate_first.person.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

}
