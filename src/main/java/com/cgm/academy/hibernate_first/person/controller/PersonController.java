package com.cgm.academy.hibernate_first.person.controller;

import com.cgm.academy.hibernate_first.person.domain.Person;
import com.cgm.academy.hibernate_first.person.service.PersonService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@NoArgsConstructor
@Controller
public class PersonController {

    private PersonService personService;

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void savePerson(Person person) {
        this.personService.save(person);
    }

    public List<Person> getAllPerson() {
        return personService.getAllPerson();
    }
} 