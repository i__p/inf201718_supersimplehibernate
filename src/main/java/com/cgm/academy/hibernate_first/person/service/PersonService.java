package com.cgm.academy.hibernate_first.person.service;

import com.cgm.academy.hibernate_first.person.domain.Person;
import com.cgm.academy.hibernate_first.person.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Slf4j
@Service
public class PersonService {
    private PersonRepository personRepository;

    @Autowired
    public void setPersonRepository(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPerson() {
        return StreamSupport.stream( personRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public void save(Person person) {
        log.info("Saving person: {}", person);
        personRepository.save(person);
    }

}
